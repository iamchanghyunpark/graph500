# Graph500 2.1.4 fixes for precompiled graphs

## Generate the graph and Root file
To generate graphs use the make-edgelist file.

```
$ ./make-edgelist -s 24 -e 16 -o graph.dump -r root.dump
```

You can also use the -n/-N option to specify how many roots you want to
generate. (This results in how many BFS iterations are done).

## Run from pregenerated graphs
To run the based on the generated graph sizes:

```
seq-list/seq-list -o graph.dump -r root.dump -s 24 -e 16
```

Remember to give the same scale and edge factors as when you generated the
graph.

